<?php

namespace Initial\Make\Console\Commands;

use Illuminate\Console\Command;

use Initial\Make\Generator\ModuleGenerator;

class MakeModule extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'module:make {name}';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Create new module';

    /**
    * Execute the console command.
    *
    * @return mixed
    */
    public function handle()
    {
        $name = $this->argument('name');

        new ModuleGenerator($name, base_path('modules'), $this);
    }
}