<?php
namespace Initial\Make;

use \Illuminate\Support\ServiceProvider;

class MakeServiceProvider extends ServiceProvider
{
    /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
    protected $defer = true;

    public function boot()
    {
        //
    }

    public function register()
    {
        $this->commands([
            \Initial\Make\Console\Commands\MakeModule::class
        ]);
    }
}
