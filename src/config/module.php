<?php

return [
    "Database" => [
        "Migrations" => [
            ".gitkeep"
        ],
        "Seed" => [
            ".gitkeep"
        ],
    ],
    "Providers" => [
        ".gitkeep"
    ],
    "Http" => [
        "Controllers" => [
            ".gitkeep"
        ],
        "Middleware" => [
            ".gitkeep"
        ],
        "Request" => [
            ".gitkeep"
        ],
        "Routes" => [
            "api.php.txt",
            "web.php.txt",
            "console.php.txt"
        ]
    ],
    "ServiceProvider.php.txt"
];
