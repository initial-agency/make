<?php

namespace Initial\Make\Generator;

use Illuminate\Console\Command;

class ModuleGenerator extends Generator
{


    public function __construct(string $name, string $path, Command $command)
    {
        parent::__construct($name, $path, 'module', $command);

        $this->generate();
    }

    protected function generate()
    {
        if (!$this->ifExist($this->getName())) {
            // Create Module Folder
            $this->filesystem->makeDirectory($this->getPath().'/'.$this->getName());

            foreach ($this->config as $key => $value) {
                if (is_array($value)) {
                    $this->generateFolder($key, $value);
                } else {
                    $this->generateFile($value, $this->getPath().'/'.$this->getName().'/');
                }
            }
            $this->command->info("Module [{$this->getName()}] created successfully.");
        } else {
            $this->command->error("Module [{$this->getName()}] already exist.");
        }
    }


}

