<?php

namespace Initial\Make\Generator;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

/**
 * Class Generator
 * @package Initial\Make\Generator
 */
class Generator
{
    /**
     * All Componenents in directory
     *
     * @var array
     */
    protected $components;

    /**
     * The Path of directory
     *
     * @var string
     */
    protected $path;

    /**
     * Command instance
     *
     * @var \Illuminate\Console\Command
     */
    protected $command;

    /**
     * Filesystem Instance
     *
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * Config
     *
     * @var array
     */
    protected $config;

    /**
     * Type of Generator
     *
     * @var string
     */
    protected $type;

    /**
     * Component Name
     *
     * @var string
     */
    protected $name;

    /**
     * Generator constructor.
     *
     * @param string $name
     * @param string $path
     * @param string $type
     * @param Command $command
     */
    public function __construct(string $name, string $path, string $type, Command $command)
    {
        $this->filesystem = new Filesystem();
        $this->setName($name);
        $this->path = $path;
        $this->type = $type;
        $this->command = $command;

        $this->setComponents();
        $this->loadConfig();
    }

    /**
     * @return array
     */
    public function getComponents(): array
    {
        return $this->components;
    }

    protected function setComponents()
    {
        $components = $this->filesystem->directories($this->getPath());

        foreach ($components as $component) {
            $this->components[] = basename($component);
        }
    }

    protected function loadConfig()
    {
        $this->config = require_once dirname(__DIR__).'/config/'.$this->getType().'.php';
    }

    /**
     * @param $folder
     * @param $content
     * @param null $path
     */
    protected function generateFolder($folder, $content, $path = null)
    {
        if (!$path) {
            $path = $this->getPath() . '/'. $this->getName();
        }

        $this->filesystem->makeDirectory($path .'/'. $folder);

        foreach ($content as $key => $value) {
            if (is_array($value)) {
                $this->generateFolder($key, $value, $path . '/' . $folder);
            } else {
                $this->generateFile($value, $path.'/'.$folder.'/');
            }
        }
    }

    protected function generateFile($filename, $path)
    {
        if ($filename == '.gitkeep') {
            $this->filesystem->put($path . '.gitkeep', '');
        } else {
            switch ($this->type) {
                case 'module':
                    $content = $this->filesystem->get(dirname(__DIR__) . '/Templates/' . $this->type . '/' . $filename);
                    $content = str_replace('%MODULE_NAME%', $this->getName(), $content);
                    $name = $this->filesystem->name(dirname(__DIR__) . '/Templates/module/' . $filename);
                    $this->filesystem->put($path . $name, $content);
                    break;
            }
        }
    }

    protected function getName()
    {
        return ucfirst($this->name);
    }

    protected function setName($name)
    {
        $this->name = Str::studly($name);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    protected function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param $name
     * @return bool
     */
    protected function ifExist($name): bool
    {
        return in_array($name, $this->components);
    }
}
